from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Annida Safira Arief' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000,1,22)
npm = 1706040050 # TODO Implement this

temanDepan = 'Patricia Anugrah Setiani'
umurTemanDepan = int(datetime.now().strftime("%Y"))
tanggalLahirTemanDepan = date(1999,12,15) #TODO Implement this, format (Year, Month, Date)
npmTemanDepan = 1706074884 # TODO Implement this

temanBelakang = 'Lulu Ilmaknun Qurotaini'
umurTemanBelakang = int(datetime.now().strftime("%Y"))
tanggalLahirTemanBelakang = date(1999,8,26) #TODO Implement this, format (Year, Month, Date)
npmTemanBelakang = 1706979341 # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'nameTD': temanDepan, 'ageTD': calculate_age(tanggalLahirTemanDepan.year), 'npmTD': npmTemanDepan,'nameTB': temanBelakang, 'ageTB': calculate_age(tanggalLahirTemanBelakang.year), 'npmTB': npmTemanBelakang}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
